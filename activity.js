// db.courses.insertMany([

// 

// 

// 

//     {

// 

//         "name": "HTML Basics",

// 

//         "price": 20000,

// 

//         "isActive": true,

//         

//         "instructor": "Sir Alvin"

// 

//     },

// 

//     {

// 

//         "name": "CSS 101 + Flexbox",

// 

//         "price": 21000,

// 

//         "isActive": true,

//         

//         "instructor": "Sir Alvin"

// 

// 

//     },

// 

//     {

// 

//         "name": "Javascript 101",

// 

//         "price": 32000,

// 

//         "isActive": true,

//         

//         "instructor": "Ma'am Tine"

// 

// 

//     },

// 

//     {

// 

//         "name": "Git 101, IDE and CLI",

// 

//         "price": 1900,

// 

//         "isActive": false,

//         

//         "instructor": "Ma'am Tine"

// 

// 

//     },

// 

//     {

// 

//         "name": "React.JS 101",

// 

//         "price": 25000,

// 

//         "isActive": true,

//         

//         "instructor": "Ma'am Miah"

// 

// 

//     },

// 

// 

// 

// ])

// 1.

db.courses.find({instructor:'Sir Alvin',price:{$gte:10000}},{_id:0,name:1,price:1})



// 2.

db.courses.find({instructor:"Ma'am Tine",isActive:false},{_id:0,name:1,price:1})



// 3.

db.courses.find({$and:[{name:{$regex: 'r',$options: '$i'}},{price:{$lte:25000}}]})



// 4.

db.courses.updateMany({"price":{$lt:21000}},{$set:{"isActive":false}})

// 5.

db.courses.deleteMany({"price":{$gte:25000}}) 

