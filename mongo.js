// db.products.insertMany([
// 
//     {
//         "name": "Iphone X",
//         "price": 30000,
//         "isActive": true
//     },
//     {
//         "name": "Samsung Galaxy S21",
//         "price": 51000,
//         "isActive": true
//     },
//     {
//         "name": "Razer Blackshark V2X",
//         "price": 2800,
//         "isActive": false
//     },
//     {
//         "name": "RAKK Gaming Mouse",
//         "price": 1800,
//         "isActive": true
//     },
//     {
//         "name": "Razer Mechanical Keyboard",
//         "price": 4000,
//         "isActive": true
//     },
// 
// ])
// 
// db.users.insertMany([
// 
//     {
//         "firstname": "Mary Jane",
//         "lastname": "Watson",
//         "email": "mjtiger@gmail.com",
//         "password": "tigerjackpot15",
//         "isAdmin": false
//     },
//       {
//         "firstname": "Gwen",
//         "lastname": "Stacy",
//         "email": "stacyTech@gmail.com",
//         "password": "stacyTech1991",
//         "isAdmin": true
//     },  
//     {
//         "firstname": "Peter",
//         "lastname": "Parker",
//         "email": "peterWetDev@gmail.com",
//         "password": "webdeveloperPeter",
//         "isAdmin": true
//     },
//       {
//         "firstname": "Jonnah",
//         "lastname": "Jameson",
//         "email": "jjjameson@gmail.com",
//         "password": "spideyisamence",
//         "isAdmin": false
//     },
//       {
//         "firstname": "Otto",
//         "lastname": "Octavius",
//         "email": "ottoOctopi@gmail.com",
//         "password": "docOck15",
//         "isAdmin": true
//     },
// 
// ])
// 
 
// db.users.find({firstname:{$regex: '0'}})
// 
// db.users.find({firstname:{$regex: '0',$options: '$i'}})
// used our regec will be case insensitive

// You can also find for documents with parial matches
// db.products.find({name:{$regex: 'phone', $options: '$i'}})

// Find users whose email have the word "web" in it
// db.users.find({email:{$regex: 'web', $options: '$i'}})



// db.products.find({name:{$regex: 'Razer', $options: '$i'}})
// db.products.find({name:{$regex: 'RAKK', $options: '$i'}}) 



// $or $and - logical Operators - works almost the same way as they in JS
//$or - allows us to have a logical operation to fond for documents which
// can satisfy at least 1 of our conditions


// $or
// db.products.find({$or:[{name:{$regex: 'x',$options: '$i'}},{price:{$lte:10000}}]})
// db.products.find({$or:[{name:{$regex: 'x',$options: '$i'}},{price:{$gte:30000}}]})

//$and
// look or find for documents that satisfies both conditions

// db.products.find({$and:[{name:{$regex: 'Razer',$options: '$i'}},{price:{$gte:3000}}]})
// db.products.find({$and:[{name:{$regex: 'x',$options: '$i'}},{price:{$gte:30000}}]})
// db.users.find({$and:[{lastname:{$regex: 'w',$options: '$i'}},{isAdmin:false}]})
// db.users.find({$and:[{firstname:{$regex: 'a',$options: '$i'}},{isAdmin:true}]})



// Field Projection - allows us to show/hide certain properties/fields
// db.collection.find({query},{projection}) - 0 means hide, 1 is show

// db.users.find({},{_id:0,password:0})

// db.users.find({isAdmin:true},{_id:0,email:1})
// _id fields must be explicitly hidden

// We can also just pick which fields to show.
// db.users.find({isAdmin:false},{firstname:1,lastname:1})

db.products.find({price:{$gte:10000}},{_id:0,name:1,price:1})




